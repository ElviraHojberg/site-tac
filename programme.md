---
layout: text-page

title: 'Programme'

permalink: /programme

img: ./assets/img/programme.jpeg

waiting: false
---

<section id='ambition'>
<h2>Bifurquer pour un avenir soutenable</h2>
 
<p>L’ère Anthropocène se caractérise par la rupture de tous les grands équilibres naturels (réchauffement climatique, érosion de la biodiversité etc.) sous l’effet des activités humaines. Cependant l’héritage philosophique de Bernard Stiegler nous conduit à penser que, derrière les activités humaines, ce qui est fondamentalement en cause dans cette crise planétaire est un certain rapport de l’Homme à la technique devenue technologie. Transport, agriculture, construction … : en deux siècles, chaque domaine d’activité humaine a été considérablement transformé par l’invention et la mobilisation de nouveaux outils techniques. La lecture de cette transformation, d’abord majoritairement perçue comme progrès, semble désormais s’inverser alors que se révèlent avec éclat ses aspects toxiques : l’humanité subit, aussi, les conséquences d’une évolution technologique dont elle a pour partie perdu la matrise.</p>

<p>Dans ce contexte, bifurquer demande un autre mode de développement, fondé sur une réappropriation par les habitants du milieu technologique qui les entoure, afin d’en limiter la toxicité. La thèse que nous défendons est que cette réappropriation nécessite l’élaboration de nouveaux savoirs pouvant être aussi bien théoriques que relevant d’un savoir-faire ou d’un savoir-vivre. Bien qu’existant sous de multiples formes, le savoir a pour nous une signification très précise. Il est ce qui relie des personnes au sein de pratiques collectives dès lors que se vérifie une double dynamique où chaque individu contribue à transformer cette pratique à partir des singularités qu’il apporte ; réciproquement, la pratique cultivée en commun permet à chacun de développer sa propre singularité. En ce sens, en complément des logiques de formation qui visent à l’acquisition de compétences préexistantes, l’élaboration de ces nouveaux savoirs passe par des démarches collectives de capacitation qui, pour être solvable, doivent pouvoir s’inscrire dans un nouveau modèle économique, l’économie de la contribution. Aussi, c’est à construire cette économie de la contribution et à mettre en oeuvre des démarches de capacitation avec les habitants que s’attelle le programme TAC.</p>
</section>
 
<section id='methode'>
<h2>Préparer et expérimenter une économie de la contribution</h2>
 
<p>L’économie de la contribution vise à assurer une pérennité aux démarches de capacitation, en rendant possible la participation des habitants par l’octroi d’un revenu contributif conditionnel, conçu comme une rémunération hors emploi. Par ce biais, l’économie de la contribution souhaite créer les conditions d’une transformation systémique, où les habitants seront encouragés à développer collectivement des savoirs, puis à les mettre au service de leur territoire, créant ainsi des “territoires apprenants contributifs’. Cette mutation de l’économie locale en une économie du soin basée sur le savoir permettra l’émergence de nouveaux emplois contributifs.  Comme dans le système des intermittents du spectacle, un habitant pourra alors renouveler son revenu contributif en occupant un emploi contributif pour une durée déterminée (qui sera délibérée localement). </p>  

<p>Une telle expérimentation, pour être vraiment démonstrative, suppose un travail de préparation de terrain. Il est ainsi nécessaire de mener une enquête permanente pour identifier les dynamiques territoriales susceptibles de s’inscrire dans une économie de la contribution. Il s’agit ensuite de réunir les acteurs qui se sentent concernés par une même dynamique dans des ateliers de capacitation. Ces ateliers articulent à la fois la recherche et l’expérimentation sur le terrain de nouvelles pratiques, ce qui permet de nouveaux savoirs. Ils s’appuient en cela sur la méthode de la recherche contributive, qui consiste à faire travailler ensemble habitants, professionnels et chercheurs universitaires dans des projets conçus pour offrir une égale possibilité à chaque participant de contribuer à la dynamique de recherche, et de devenir en ce sens lui-même chercheur et acteur de son avenir.</p> 

<p>L’objectif du programme TAC consiste à poursuivre l’élaboration et à expérimenter cette économie de la contribution en Seine-Saint-Denis. </p> 
</section>

<section id='origine'>
<h2>D’où vient le TAC ?</h2>
 
<p>Ce programme hérite des réflexions de l’IRI et d’Ars Industrialis sur l’économie de la contribution et s’est concrétisé en 2016 par un dialogue approfondi entre Bernard Stiegler et l’équipe de l’IRI et Patrick Braouezec, alors président de l’EPT Plaine Commune et les élus de ce territoire. Depuis le début des années 2000 et sur la base des travaux philosophiques de Bernard Stiegler, l’IRI et Ars Industrialis travaillent sur les multiples enjeux ouverts par le basculement de nos sociétés dans les technologies numériques. À partir de 2010, les réflexions se sont orientées vers le champ économique, notamment autour de la création d’un nouveau modèle économique et industriel, fondé sur le passage d’une économie de la consommation à une économie de la contribution. C’est sur ce socle historique et théorique qu’a été conçu le programme TAC, porté aujourd’hui par l’équipe de l’IRI dans l’ensemble de la Seine-Saint-Denis.</p>
</section>
 