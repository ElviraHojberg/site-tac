---
layout : text-page

title: 'La clinique contributive'

permalink : /capacitation/clinique

class: 'clinique'

waiting: false
---

<section id="resume">
<h2>La clinique contributive en résumé</h2>

<p>Les études scientifiques, notamment internationales, démontrent les effets très toxiques (retard de langage, des apprentissages, de la motricité et troubles du comportement) de la surexposition des très jeunes enfants (0-3 ans) aux écrans (smartphones, tablettes, ordinateurs, télévision).</p>

<p>Pour répondre de façon innovante à ce problème de santé public trop peu (re)connu, l’IRI a créé depuis novembre 2019 la clinique contributive, un dispositif expérimental mis en œuvre dans et avec l’équipe des professionnelles du centre de PMI (protection maternelle et infantile) Pierre Sémard, à Saint-Denis.</p>

<p>Dans ce cadre, un « atelier sur les écrans » réunit, une matinée tous les 15 jours, des parents concernés (dont certains ayant fortement surexposé leur enfant), les professionnelles de la PMI et des chercheurs de l’IRI (pédopsychiatre, philosophe et biologiste). En analysant individuellement et collectivement leur pratique numérique, en partageant leurs expériences de vie, pratiques professionnelles et savoirs académiques, les membres de l’atelier élabore progressivement un savoir commun sur les écrans. Ce savoir permet la naissance d’un regard critique sur les écrans et des changements de pratiques individuelles et collectives.</p>

<p>Cet atelier est articulé avec un séminaire ouvert à des professionnels extérieurs, qui vient approfondir les questions théoriques.</p>
</section>

<section id="contexte">
<h2>Contexte</h2>

<p>En mai 2017, dans une tribune du journal Le Monde, des médecins et professionnels de la petite enfance alertaient sur « des graves effets d’une exposition massive et précoce des bébés et des jeunes enfants à tous types d’écrans : smartphone, tablette, ordinateur, console, télévision. Nous recevons de très jeunes enfants stimulés principalement par les écrans, qui, à 3 ans, ne nous regardent pas quand on s’adresse à eux, ne communiquent pas, ne parlent pas, ne recherchent pas les autres, sont très agités ou très passifs ». 
Depuis, les études scientifiques, notamment internationales, se sont succédées pour souligner et préciser les impacts d’une surexposition aux écrans chez les enfants de moins de 3 ans. Elle impacte le développement des enfants de plusieurs manières et notamment :<br>
◦ Elle perturbe la relation parent-enfant (absence de disponibilité des parents absorbés par leur smartphone) or cette relation joue un rôle essentiel dans les apprentissages.<br>
◦ Elle entraine une perte d’expériences des enfants dans le monde réel, or ces expériences, basées sur l’exercice de leur cinq sens, sont fondamentales pour leur développement.<br>
◦ Elle entrave le développement de l’attention profonde, indispensable aux apprentissages.<br> 
Au final, de 0 à 3 ans, l’exposition aux écrans n’apporte aucun bénéfice pour l’enfant ; en revanche, le temps d’écrans à 2 ans est corrélé à des troubles du sommeil, des troubles des apprentissages (langage et mathématiques), des troubles de l’attention et du comportement (étude française ELFE, rapport 2021) à 3 ans et à 5 ans 1/2. Les études anglo saxonnes plus nombreuses rapportent des retards de développement, des troubles de l’attention, des troubles psycho-affectifs, corrélés au temps d’écrans.</p>

<p>La diffusion massive des écrans mobiles est encore très récente (2008 pour les smartphones, 2013 pour les tablettes). Elle crée une situation de disruption, caractérisée par le fait que les institutions sociales n’ont pas le temps de s’approprier la technologie, qui les court-circuite et ce faisant, les déstructure. Ce processus est en cours dans le domaine de l’éducation, profondément déstabilisée par l’irruption des écrans. L’objectif de la Clinique contributive est donc d’inventer collectivement de nouvelles pratiques éducatives adaptées au monde numérique.</p>

<p>La surexposition aux écrans touchent de très nombreuses familles, de tous les milieux sociaux. Elle est cependant particulièrement répandue dans les milieux les plus défavorisés, très présents en Seine-Saint-Denis, du fait de plusieurs facteurs :<br> 
◦ Les familles y sont souvent moins bien informées des dangers des écrans.<br>
◦ Vivant dans des espaces plus contraints, le recours aux écrans permet d’ « occuper sans bruit » les enfants.</p>
</section>


<section id="methode">
<h2>Méthode : recherche contributive et psychothérapie institutionnelle</h2>

<p>La démarche de la Clinique contributive articule :<br>
◦ Une démarche inspirée de la psychothérapie institutionnelle et des groupes d’entraide mutuelle, selon lesquels le patient se soigne en prenant soin de l’institution et des autres patients, devenant ainsi lui-même soignant.<br>
◦ Une démarche provenant de la recherche contributive, qui articule la recherche fondamentale aux problèmes concrets rencontrés par les habitants d’un territoire, qui deviennent ainsi eux-mêmes chercheurs en participant activement à l’évolution des disciplines théoriques et des pratiques professionnelles existantes. Cette démarche vise notamment à accélérer la réponse de la société face à la rapidité des changements techniques.</p>

<p>Il s’agit donc pour les parents participant à la clinique contributive de devenir à la fois « soignants » et « chercheurs », c’est à dire :<br>
◦ De partager entre eux leurs savoirs (savoirs numériques, pratiques des outils numériques, savoirs éducatifs, pratiques d’habitation, savoirs culinaires, pratiques alimentaires), qui sont autant de manière de prendre soin de soi et des autres en prenant soin d’un milieu commun.<br>
◦ De concevoir et de mettre en œuvre de nouvelles thérapeutiques permettant de faire face aux enjeux de la surexposition aux écrans, en étroite coopération avec les chercheurs et les professionnels.</p> 

<p>Cet atelier permet à certains parents de devenir des « ambassadeurs » de leurs nouveaux savoirs sur les écrans et de les partager avec d’autres parents, notamment lors de rencontres publiques organisées par des municipalités ou des structures d’accueil de la petite enfance.</p>  
</section>


<section id="horizon">
<h2>Horizon</h2>
<p>Dans le cadre du programme Territoire Apprenant Contributif, il s’agira, à terme, de reconnaître la valeur économique de la capacitation et des nouveaux savoirs des parents dans le cadre d’un futur métier d’ « ambassadeur d’un numérique raisonné », et de mettre en œuvre sa rémunération à travers un revenu contributif lié à la création d’emplois intermittents.</p>
</section>


<section id="resultats">
<h2>Historique et premiers résultats</h2>
<p>De novembre 2018 à novembre 2019, la Clinique contributive a alterné deux types de rencontres :</p> 

<p><b>1)</b> Les « Ateliers sur les écrans » ont réuni tous les quinze jours l’équipe de la PMI, des professionnels du soin des enfants, des chercheurs et, à partir d’avril 2019, des parents ayant surexposé leurs enfants aux écrans et ayant trouvé les ressorts et l’énergie de les sortir de la surexposition. Durant ces rencontres, les membres du groupe ont échangé sur leur pratique et savoirs et ont peu à peu élaboré un savoir commun autour des processus des processus de developpement de l’enfant, biologiques et psychiques, de 0 à 3 ans et des processus de captologie et d’exploitation de nos vulnérabilités psychologiques par les industriels du numériques.<br>
<b>2)</b> Pour approfondir ces savoirs, des séminaires ont réuni mensuellement des chercheurs, ainsi que des membres extérieurs à « l’atelier sur les écrans », intéressés par l’élaboration théorique de la question de la surexposition enfantine. Y ont été lu et discuté des textes des théoriciens qui inspirent la Clinique contributive : Jeu et réalité de D. Winnicott, Qu’est-ce-que les Lumières? et les principes de la méthode des Alcooliques Anonymes, de Grégory Bateson, La notion de club thérapeutique, de Yves Teulié, L’enseignement de Tosquelles, de Jacques Delion, et la question de la psychothérapie institutionnelle, Au-delà du principe de plaisir et Le moi et le ça, de Sigmund Freud et les concepts de base de la psychanalyse, Philosophy and Other Addictions ; Le pharmakon, le dopominage et la société addictogène, de Gerald Moore, Le rôle de l'école dans le développement libidinal de l'enfant, de Jacques Arveiller et la question de l’écriture chez Mélanie Klein.</p>

<p>Depuis novembre 2019, l’ « atelier sur les écrans » est ouvert aux parents intéressés. Il a lieu une fois toutes les deux semaines, le vendredi matin et dure de 9h30 à 11h. Les très jeunes enfants peuvent également être accueillis. Ils sont alors pris en charge par une professionnelle de la PMI mais restent dans la même pièce que le groupe. Assis sur des chaises disposées en cercle afin de favoriser l’horizontalité des échanges, les membres du groupe visionnent ensemble de courtes vidéos, à partir desquelles ils échangent sur leur pratiques, difficultés, savoirs.</p> 

<p>Le groupe-projet se réunit ensuite de 11h à 12h pour analyser la séance et préparer l’Atelier suivant. Tous les deux mois environ, un séminaire, dans les locaux de la PMI, approfondit des thèmes choisis par les membres du groupe.</p>

<p>Quelques mois après l’ouverture aux parents de l’”atelier sur les écrans”, le premier confinement est intervenu, restreignant fortement pour les parents l’accès à la PMI. Ces nouvelles conditions de fonctionnement ont interrompu la dynamique d’élargissement du groupe à des parents. Le groupe-projet a profité de cette période pour analyser l’impact du confinement sur ses membres et pour travailler, à partir de son expérience, à une méthodologie de formation des professionnels de la petite enfance à la thématique des écrans. Cette formation sera mise en oeuvre en 2022 auprès de professionnels de la petite enfance de Saint-Denis. 
Depuis octobre 2021, la PMI se réouvre aux parents, permettant à l’atelier d’impuler une dynamique de création d’un groupe de parents capacités sur les écrans et en mesure d’inventer ensemble comment éduquer les enfants, entourés par les écrans.</p>
</section>

